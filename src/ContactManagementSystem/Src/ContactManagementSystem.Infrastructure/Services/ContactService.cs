﻿using ContactManagementSystem.Domain.Entities;
using ContactManagementSystem.Domain.SeedWork;
using ContactManagementSystem.Infrastructure.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagementSystem.Infrastructure.Services
{
    public class ContactService : IContactService
    {
        private readonly IContactRepository _contactRepository;

        public ContactService(IContactRepository contactRepository)
        {
            _contactRepository = contactRepository;
        }

        public Contact Create(Contact entity)
        {
            return _contactRepository.Create(entity);
        }

        public IList<Contact> Get()
        {
            return _contactRepository.Get();
        }

        public Contact Get(int id)
        {
            return _contactRepository.Get(id);
        }

        public void Remove(Contact entity)
        {
            _contactRepository.Remove(entity);
        }

        public void Remove(int id)
        {
            _contactRepository.Remove(id);
        }

        public Contact Update(Contact entity)
        {
            return _contactRepository.Update(entity);
        }

    }

    public interface IContactService : IEntityService<Contact>
    {
    }
}
