﻿using ContactManagementSystem.Domain.Entities;
using ContactManagementSystem.Domain.SeedWork;
using ContactManagementSystem.Infrastructure.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagementSystem.Infrastructure.Services
{
    public class CompanyService : ICompanyService
    {
        private readonly ICompanyRepository _companyRepository;

        public CompanyService(ICompanyRepository companyRepository)
        {
            _companyRepository = companyRepository;
        }

        public Company Create(Company entity)
        {
            return _companyRepository.Create(entity);
        }

        public IList<Company> Get()
        {
            return _companyRepository.Get();
        }

        public Company Get(int id)
        {
            return _companyRepository.Get(id);
        }

        public void Remove(Company entity)
        {
            _companyRepository.Remove(entity);
        }

        public void Remove(int id)
        {
            _companyRepository.Remove(id);
        }

        public Company Update(Company entity)
        {
            return _companyRepository.Update(entity);
        }
    }

    public interface ICompanyService : IEntityService<Company>
    {
    }
}
