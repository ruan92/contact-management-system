﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagementSystem.Infrastructure.Data
{
    public class DbContext : IDbContext
    {
        public string CompaniesDocument()
        {
            var baseDir = AppContext.BaseDirectory;
            var _companiesJsonPath = "Data\\Database\\companies.json";
            _companiesJsonPath = Path.Join(baseDir, _companiesJsonPath);
            return _companiesJsonPath;
        }

        public string ContactsDocument()
        {
            var baseDir = AppContext.BaseDirectory;
            var _contactsJsonPath = "Data\\Database\\contacts.json";
            _contactsJsonPath = Path.Join(baseDir, _contactsJsonPath);
            return _contactsJsonPath;
        }
    }

    public interface IDbContext
    {
        /// <summary>
        /// Returns the path to the contacts document.
        /// </summary>
        /// <returns>The contacts document path.</returns>
        string ContactsDocument();
        /// <summary>
        /// Returns the path to the companies document.
        /// </summary>
        /// <returns>The companies document path.</returns>
        string CompaniesDocument();
    }
}
