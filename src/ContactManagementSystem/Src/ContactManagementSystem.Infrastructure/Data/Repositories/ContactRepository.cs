﻿using ContactManagementSystem.Domain.Entities;
using ContactManagementSystem.Domain.SeedWork;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UseExceptionHandlerAlternative.Middleware.GlobalErrorHandling.Exceptions;

namespace ContactManagementSystem.Infrastructure.Data.Repositories
{
    public class ContactRepository : IContactRepository
    {
        readonly IDbContext _dbContext;
        public ContactRepository(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Contact Create(Contact entity)
        {
            var contacts = Get();
            var lastContact = contacts.LastOrDefault();
            var lastId = 0;
            if (lastContact != null)
                lastId = lastContact.ContactId;

            entity.ContactId = ++lastId;
            contacts.Add(entity);
            string json = JsonConvert.SerializeObject(contacts.ToArray());
            File.WriteAllText(_dbContext.ContactsDocument(), json);
            return entity;
        }

        public IList<Contact> Get()
        {
            var contacts = new List<Contact>();
            using (StreamReader r = new StreamReader(_dbContext.ContactsDocument()))
            {
                string json = r.ReadToEnd();
                var contactsDeserialized = JsonConvert.DeserializeObject<List<Contact>>(json);
                if (contactsDeserialized != null)
                    contacts = contactsDeserialized;
            }
            return contacts;
        }

        public Contact Get(int id)
        {
            using (StreamReader r = new StreamReader(_dbContext.ContactsDocument()))
            {
                string json = r.ReadToEnd();
                var contacts = JsonConvert.DeserializeObject<List<Contact>>(json);
                var contact = contacts.Where(contact => contact.ContactId == id).FirstOrDefault();
                return contact;
            }
        }

        public void Remove(Contact entity)
        {
            var json = "";
            using (StreamReader r = new StreamReader(_dbContext.ContactsDocument()))
            {
                json = r.ReadToEnd();
                var contacts = JsonConvert.DeserializeObject<List<Contact>>(json);
                if (!contacts.Remove(entity))
                    throw new CustomException("The Contact was not found in the database to remove it or failed to be removed.");
                json = JsonConvert.SerializeObject(contacts.ToArray());
            }
            File.WriteAllText(_dbContext.ContactsDocument(), json);
        }

        public void Remove(int id)
        {
            var json = "";
            using (StreamReader r = new StreamReader(_dbContext.ContactsDocument()))
            {
                json = r.ReadToEnd();
                var contacts = JsonConvert.DeserializeObject<List<Contact>>(json);
                var contact = contacts.Where(contact => contact.ContactId == id).FirstOrDefault();
                if (contact == null)
                    throw new CustomException("The Contact was not found in the database to remove it.");
                contacts.Remove(contact);
                json = JsonConvert.SerializeObject(contacts.ToArray());
            }
            File.WriteAllText(_dbContext.ContactsDocument(), json);
        }

        public Contact Update(Contact entity)
        {
            var json = "";
            using (StreamReader r = new StreamReader(_dbContext.ContactsDocument()))
            {
                json = r.ReadToEnd();
                var contacts = JsonConvert.DeserializeObject<List<Contact>>(json);
                var contactToRemove = contacts.Where(contact => contact.ContactId == entity.ContactId).FirstOrDefault();
                if (contactToRemove == null)
                    throw new CustomException("The Contact was not found in the database to update it.");
                contacts.Remove(contactToRemove);
                contacts.Add(entity);
                contacts.Sort();
                json = JsonConvert.SerializeObject(contacts.ToArray());
            }
            File.WriteAllText(_dbContext.ContactsDocument(), json);
            return entity;
        }
    }

    public interface IContactRepository : IRepository<Contact>
    {
    }
}
