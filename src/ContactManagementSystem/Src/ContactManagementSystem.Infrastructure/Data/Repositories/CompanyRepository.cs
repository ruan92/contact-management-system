﻿using ContactManagementSystem.Domain.Entities;
using ContactManagementSystem.Domain.SeedWork;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UseExceptionHandlerAlternative.Middleware.GlobalErrorHandling.Exceptions;

namespace ContactManagementSystem.Infrastructure.Data.Repositories
{
    public class CompanyRepository : ICompanyRepository
    {
        readonly IDbContext _dbContext;

        public CompanyRepository(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Company Create(Company entity)
        {
            throw new NotImplementedException();
        }

        public IList<Company> Get()
        {
            var companies = new List<Company>();
            using (StreamReader r = new StreamReader(_dbContext.CompaniesDocument()))
            {
                string json = r.ReadToEnd();
                var companiesDeserialized = JsonConvert.DeserializeObject<List<Company>>(json);
                if (companiesDeserialized != null)
                    companies = companiesDeserialized;
            }
            return companies;
        }

        public Company Get(int id)
        {
            using (StreamReader r = new StreamReader(_dbContext.CompaniesDocument()))
            {
                string json = r.ReadToEnd();
                var companies = JsonConvert.DeserializeObject<List<Company>>(json);
                var company = companies.Where(company => company.CompanyId == id).FirstOrDefault();
                return company;
            }
        }

        public void Remove(Company entity)
        {
            var json = "";
            using (StreamReader r = new StreamReader(_dbContext.CompaniesDocument()))
            {
                json = r.ReadToEnd();
                var companies = JsonConvert.DeserializeObject<List<Company>>(json);
                if (!companies.Remove(entity))
                    throw new CustomException("The Company was not found in the database to remove it or failed to be removed.");
                json = JsonConvert.SerializeObject(companies.ToArray());
            }
            File.WriteAllText(_dbContext.CompaniesDocument(), json);
        }

        public void Remove(int id)
        {
            var json = "";
            using (StreamReader r = new StreamReader(_dbContext.CompaniesDocument()))
            {
                json = r.ReadToEnd();
                var companies = JsonConvert.DeserializeObject<List<Company>>(json);
                var company = companies.Where(company => company.CompanyId == id).FirstOrDefault();
                if (company == null)
                    throw new CustomException("The Company was not found in the database to remove it.");
                companies.Remove(company);
                json = JsonConvert.SerializeObject(companies.ToArray());
            }
            File.WriteAllText(_dbContext.CompaniesDocument(), json);
        }

        public Company Update(Company entity)
        {
            var json = "";
            using (StreamReader r = new StreamReader(_dbContext.CompaniesDocument()))
            {
                json = r.ReadToEnd();
                var companies = JsonConvert.DeserializeObject<List<Company>>(json);
                var companyToRemove = companies.Where(company => company.CompanyId == entity.CompanyId).FirstOrDefault();
                if (companyToRemove == null)
                    throw new CustomException("The Company was not found in the database to update it.");
                companies.Remove(companyToRemove);
                companies.Add(entity);
                companies.Sort();
                json = JsonConvert.SerializeObject(companies.ToArray());
            }
            File.WriteAllText(_dbContext.CompaniesDocument(), json);
            return entity;
        }
    }

    public interface ICompanyRepository : IRepository<Company>
    {
    }
}
