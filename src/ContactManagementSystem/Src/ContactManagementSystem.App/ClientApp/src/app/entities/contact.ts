import { Company } from "../interfaces/company";
import { IContact } from "../interfaces/contact";

export class Contact implements IContact{
  contactId!: number;
  name!: string;
  jobTitle!: String;
  emailAddress!: string;
  phoneNumber!: string;
  address!: string;
  lastDateContacted!: string;
  comments!: string;
  company!: Company;

  constructor() {

  }
}
