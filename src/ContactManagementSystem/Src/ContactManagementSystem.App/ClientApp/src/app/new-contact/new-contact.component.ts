import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { Company } from '../interfaces/company';
import { Contact } from '../entities/contact';
import { CompanyService } from '../services/company.service';
import { ContactsService } from '../services/contacts.service';
import { Router, ActivatedRoute } from '@angular/router';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-new-contact',
  templateUrl: './new-contact.component.html',
  styleUrls: ['./new-contact.component.css']
})
export class NewContactComponent implements OnInit {

  contact: Contact = new Contact();

  _companies: Company[] = [];

  _selectedCompany?: Company;

  constructor(
    private companyService: CompanyService,
    private contactsService: ContactsService,
    private router: Router,
    private route: ActivatedRoute,
    @Inject(LOCALE_ID) private locale: string
  ) { }

  ngOnInit(): void {

    this.initialize();
  }

  onCompanyChange(company: Company) {
    this._selectedCompany = company;
    this.contact.company = this._selectedCompany;
  }

  onSave() {
    if (!this.contact.contactId) {
      console.log("Saving a new contact.");
      this.contactsService.saveContact(this.contact).then(observable =>
        observable.toPromise().then(contact => {
          this.router.navigateByUrl("");
        }, error => alert("The system failed to created and save a new contact."))
      );
    } else {
      console.log("Updating an existing contact.");
      this.contactsService.updateContact(this.contact).then(observable =>
        observable.subscribe(contact => {
          this.router.navigateByUrl("");
        }, error => alert("The contact failed to update and save the existing contact."))
      );
    }
  }

  initialize() {

    // Get all the companies from the companies service
    this.companyService.getCompanies().then(observable =>
      observable.subscribe((data: Company[]) => {
        this._companies = data;
        this._selectedCompany = data[0];
        this.route.params.subscribe(params => {
          // (+) converts string 'id' to a number
          let id = +params['id'];
          if (id) {
            this.contact.contactId = id;

            // Load the contact if id parameter was provided.
            this.contactsService.getContact(id).then(observable =>
              observable.subscribe(contact => {
                this.contact = contact;
                // Format date
                this.formatDate(this.contact);
                this._selectedCompany = this.contact.company;
                console.log("company to set = " + this.contact.company.companyName);
              })
            );
          }
        });
      })
    );

  }

  formatDate(contact: Contact): void {
    this.contact.lastDateContacted = formatDate(contact.lastDateContacted, 'yyyy-MM-dd', this.locale);
}

}
