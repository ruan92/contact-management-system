import { Company } from "./company";

export interface IContact {
  contactId: number;
  name: string;
  jobTitle: String;
  emailAddress: string;
  phoneNumber: string;
  address: string;
  lastDateContacted: string;
  comments: string;
  company: Company;
}
