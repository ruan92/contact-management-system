export interface Config {
  /**
   * Trailing slashes are not removed and should not be included in the config.json file.
   */
  httpsApiUrl: string;
}
