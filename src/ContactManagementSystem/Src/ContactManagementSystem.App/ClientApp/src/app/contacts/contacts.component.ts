import { Component, OnInit } from '@angular/core';
import { IContact } from '../interfaces/contact';
import { Company } from '../interfaces/company';
import { ContactsService } from '../services/contacts.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {

  contacts: IContact[] = [];

  constructor(
    private contactsService: ContactsService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.getContacts();
  }

  onRowClick(contact: IContact) {
    this.router.navigate(['/contact', contact.contactId]);
    //this.router.navigateByUrl(`contact/${contact.contactId}`)
}

  /**
   * Get all the contacts from the contacts service
   */
  getContacts() {
    this.contactsService.getContacts().then(observable =>
      observable.subscribe((data: IContact[]) => this.contacts = data, error => alert("An error occured trying to load the contacts."))
    );
  }

}
