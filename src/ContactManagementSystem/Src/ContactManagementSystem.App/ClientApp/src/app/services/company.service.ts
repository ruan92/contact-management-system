import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { Company } from '../interfaces/company';
import strings from '../strings/strings';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  constructor(
    private httpClient: HttpClient,
    private configService: ConfigService
  ) { }

 /**
 * Get all the companies from the api endpoint.
 */
  async getCompanies(): Promise<Observable<Company[]>> {
    let apiUrl;
    await this.configService.getConfig().toPromise().then(config =>
      apiUrl = config.httpsApiUrl, error => alert(strings.configFailLoad));
    return this.httpClient.get<Company[]>(`${apiUrl}/company`);
  }
}
