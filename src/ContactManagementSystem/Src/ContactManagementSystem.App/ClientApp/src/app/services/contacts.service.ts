import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { IContact } from '../interfaces/contact';
import { ConfigService } from './config.service';
import strings from '../strings/strings';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {

  constructor(
    private httpClient: HttpClient,
    private configService: ConfigService
  ) { }

  /**
   * Get all the contacts from the api endpoint.
   */
  async getContacts(): Promise<Observable<IContact[]>> {
    let apiUrl;
    await this.configService.getConfig().toPromise().then(config =>
      apiUrl = config.httpsApiUrl, error => alert(strings.configFailLoad));
    return this.httpClient.get<IContact[]>(`${apiUrl}/contact`);
  }

 /**
 * Get contact for given id from the api endpoint.
 */
  async getContact(id : number): Promise<Observable<IContact>> {
    let apiUrl;
    await this.configService.getConfig().toPromise().then(config =>
      apiUrl = config.httpsApiUrl, error => alert(strings.configFailLoad));
    return this.httpClient.get<IContact>(`${apiUrl}/contact/${id}`);
  }

  /**
   * Save the contact to the backend.
   */
  async saveContact(contact: IContact): Promise<Observable<IContact>> {
    let apiUrl;
    await this.configService.getConfig().toPromise().then(config =>
      apiUrl = config.httpsApiUrl, error => alert(strings.configFailLoad));
    return this.httpClient.post<IContact>(`${apiUrl}/contact`, contact);
  }

 /**
 * Update the contact on the backend.
 */
  async updateContact(contact: IContact): Promise<Observable<IContact>> {
    let apiUrl;
    await this.configService.getConfig().toPromise().then(config =>
      apiUrl = config.httpsApiUrl, error => alert(strings.configFailLoad));
    return this.httpClient.put<IContact>(`${apiUrl}/contact`, contact);
  }
}
