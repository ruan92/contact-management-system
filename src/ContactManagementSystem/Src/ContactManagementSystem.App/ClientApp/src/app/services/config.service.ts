import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Config } from '../interfaces/config';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  private configUrl: string = "assets/config.json";

  constructor(
    private httpClient: HttpClient
  ) { }

  /**
   * Returns an Observable of Config
   */
  getConfig() {
    return this.httpClient.get<Config>(this.configUrl);
  }
}
