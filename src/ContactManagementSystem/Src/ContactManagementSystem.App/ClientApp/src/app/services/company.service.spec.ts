import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { Company } from '../interfaces/company';
import { Config } from '../interfaces/config';

import { CompanyService } from './company.service';
import { ConfigService } from './config.service';

// Fake todos and response object
const companies: Company[] = [{
  companyId: 1,
  companyName: "HP"
},
{
  companyId: 2,
  companyName: "Microsoft"
},
{
  companyId: 3,
  companyName: "Apple"
}
];

const config: Config = {
  httpsApiUrl: ""
}

const okConfigResponse = new Response(JSON.stringify(config));

describe('CompanyService', () => {
  let service: CompanyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CompanyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('gets the companies', () => {
    // Arrange
    const httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    const configSpy = jasmine.createSpyObj('ConfigService', ['getConfig']);
    const companiesService = new CompanyService(httpClientSpy, configSpy);
    httpClientSpy.get.and.returnValue({ status: 200, data: companies });
    configSpy.getConfig.and.returnValue({ status: 200, data: config });
    // Act
    it('should return data for abc endpoint and be 3', () => {
      companiesService.getCompanies().then(observable =>
        observable.subscribe((data: Company[]) => expect(data.length).toBe(3)))
    });
    // Assert
  });
})
