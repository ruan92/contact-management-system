using ContactManagementSystem.Infrastructure.Data;
using Microsoft.OpenApi.Models;
using System.Reflection;
using Microsoft.Extensions.Options;
using ContactManagementSystem.Infrastructure.Services;
using ContactManagementSystem.Infrastructure.Data.Repositories;
using ContactManagementSystem.Domain.Entities;
using UseMiddleware.Middleware.GlobalErrorHandling.Handlers;

namespace ContactManagementSystem.App
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddControllers();

            // Add DbContext
            builder.Services.AddSingleton<IDbContext, DbContext>();

            // Add repositories.
            AddRepositories(builder);

            // Add entity services.
            AddEntityServices(builder);

            // Add Swagger to the services.
            builder.Services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Api", Version = "v1" });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);

                c.IncludeXmlComments(xmlPath);
            });

            var app = builder.Build();

            // Add exception handler middleware to the pipeline.
            // Exception middleware must be placed before other middleware in the pipeline
            // in order to catch any unhandled exceptions in the following middleware.
            app.UseMiddleware<GlobalErrorHandler>();

            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            // Add Swagger into the pipeline.
            app.UseSwagger();
            app.UseSwaggerUI(options => options.SwaggerEndpoint("/swagger/v1/swagger.json", "Api v1"));

            EnableCors(app);

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseRouting();

            app.MapControllers();

            app.Run();
        }



        #region serviceMethods

        /// <summary>
        /// Add the repositories to the application's service collection.
        /// These repositories are used for CRUD operations directly to the database.
        /// </summary>
        /// <param name="builder">The builder to add the service to.</param>
        private static void AddRepositories(WebApplicationBuilder builder)
        {
            builder.Services.AddSingleton<IContactRepository, ContactRepository>();
            builder.Services.AddSingleton<ICompanyRepository, CompanyRepository>();

        }

        /// <summary>
        /// Add the entity services to the application's service collection.
        /// These services are used for CRUD operations on the entities.
        /// </summary>
        /// <param name="builder">The builder to add the service to.</param>
        private static void AddEntityServices(WebApplicationBuilder builder)
        {
            builder.Services.AddSingleton<IContactService, ContactService>();
            builder.Services.AddSingleton<ICompanyService, CompanyService>();
        }

        /// <summary>
        /// Enable Cors for this application.
        /// </summary>
        /// <param name="app">The IApplicationBuilder instance to add Cors to.</param>
        private static void EnableCors(IApplicationBuilder app)
        {
            app.UseCors(builder =>
            {
                builder.WithOrigins("http://localhost:8080", "https://localhost:44464")
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials();
            });
        }

        #endregion
    }
}
