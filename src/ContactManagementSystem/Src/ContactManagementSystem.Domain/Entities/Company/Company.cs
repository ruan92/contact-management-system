﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagementSystem.Domain.Entities
{
    /// <summary>
    /// Company entity that contains the details of a company.
    /// </summary>
    public record Company
    {
        /// <summary>
        /// The Id and primary key.
        /// </summary>
        [Key]
        public int CompanyId { get; set; }

        /// <summary>
        /// The name of the company.
        /// </summary>
        public string CompanyName { get; set; }
    }
}
