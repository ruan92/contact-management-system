﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagementSystem.Domain.Entities
{
    /// <summary>
    /// Contact entity that contains the contact information of clients and customers
    /// </summary>
    public record Contact
    {
        /// <summary>
        /// The Id and primary key.
        /// </summary>
        //[Key]
        public int ContactId { get; set; }

        /// <summary>
        /// The full name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The job title.
        /// </summary>
        public string JobTitle { get; set; }

        /// <summary>
        /// The email address.
        /// </summary>
        public string EmailAddress { get; set; }

        /// <summary>
        /// The phone number.
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// The address.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Comments regarding the contact.
        /// </summary>
        public string Comments { get; set; }

        /// <summary>
        /// The last date the contact was contacted.
        /// </summary>
        public DateTime LastDateContacted { get; set; }

        /// <summary>
        /// The company that the contact works for.
        /// </summary>
        public Company Company { get; set; }
    }
}
