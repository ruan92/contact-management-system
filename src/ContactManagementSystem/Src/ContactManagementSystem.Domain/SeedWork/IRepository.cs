﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagementSystem.Domain.SeedWork
{
    public interface IRepository<T>
    {
        /// <summary>
        /// Return all Entities in the database.
        /// </summary>
        /// <returns>A list of all Entities in the database.</returns>
        public IList<T> Get();

        /// <summary>
        /// Return the Entity from the database that matches the given id.
        /// </summary>
        /// <param name="id">The id of the Entity to find and return.</param>
        /// <returns>An Entity that matches the given id.</returns>
        public T Get(int id);

        /// <summary>
        /// Add a new Entity to the database.
        /// </summary>
        /// <param name="entity">The Entity to add to the database.</param>
        /// <returns>The Entity that was inserted into the database.</returns>
        public T Create(T entity);

        /// <summary>
        /// Update the given Entity with the provided Entity object.
        /// </summary>
        /// <param name="entity">The Entity object that will replace the Entity.</param>
        public T Update(T entity);

        /// <summary>
        /// Remove the given Entity from the database.
        /// </summary>
        /// <param name="entity">The Entity to delete from the database.</param>
        public void Remove(T entity);

        /// <summary>
        /// Remove the Entity from the database that matches the given id.
        /// </summary>
        /// <param name="id">The id of the Entity to find the Entity to remove.</param>
        public void Remove(int id);
    }
}
