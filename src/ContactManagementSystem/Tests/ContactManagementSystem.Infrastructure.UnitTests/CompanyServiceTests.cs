﻿using ContactManagementSystem.Domain.Entities;
using ContactManagementSystem.Infrastructure.Data.Repositories;
using ContactManagementSystem.Infrastructure.Services;
using ContactManagementSystem.MockEntities;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ContactManagementSystem.Infrastructure.UnitTests
{
    public class CompanyServiceTests
    {
        [Fact]
        public void GetCompanies_Returns_The_Correct_Number_Of_Companies()
        {
            // Arrange
            var mockRepository = new Mock<ICompanyRepository>();
            mockRepository.Setup(service => service.Get()).Returns(CompaniesMock.GetCompaniesMock());

            var companyService = new CompanyService(mockRepository.Object);

            // Act
            var result = companyService.Get();

            // Assert
            var companies = Assert.IsAssignableFrom<IList<Company>>(result);
            Assert.Equal(3, companies.Count);
        }
    }
}
