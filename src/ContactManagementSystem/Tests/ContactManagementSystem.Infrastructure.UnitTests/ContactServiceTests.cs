﻿using ContactManagementSystem.Domain.Entities;
using ContactManagementSystem.Infrastructure.Data.Repositories;
using ContactManagementSystem.Infrastructure.Services;
using ContactManagementSystem.MockEntities;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ContactManagementSystem.Infrastructure.UnitTests
{
    public class ContactServiceTests
    {
        [Fact]
        public void GetContacts_Returns_The_Correct_Number_Of_Contacts()
        {
            // Arrange
            var mockRepository = new Mock<IContactRepository>();
            mockRepository.Setup(service => service.Get()).Returns(ContactsMock.GetContactsMock());

            var contactService = new ContactService(mockRepository.Object);

            // Act
            var result = contactService.Get();

            // Assert
            var contacts = Assert.IsAssignableFrom<IList<Contact>>(result);
            Assert.Equal(3, contacts.Count);
        }

        [Fact]
        public void GetContactById_Returns_A_Contact_With_The_Given_Id()
        {
            // Arrange
            var contact = new Contact()
            {
                ContactId = 3,
                Name = "Tim Cook",
                JobTitle = "CEO",
                EmailAddress = "jcook@domain.com",
                PhoneNumber = "0123789456",
                Address = "1 Apple Drive, LA",
                LastDateContacted = DateTime.Now,
                Comments = "Likes apples.",
                Company = new Company()
                {
                    CompanyId = 3,
                    CompanyName = "Apple"
                }
            };

            var mockRepository = new Mock<IContactRepository>();
            mockRepository.Setup(service => service.Get(It.IsAny<int>())).Returns(contact);

            var contactService = new ContactService(mockRepository.Object);

            // Act
            var result = contactService.Get(3);

            // Assert
            Assert.Equal(3, result.ContactId);
        }

        [Fact]
        public void UpdateContact_Returns_The_Updated_Contact()
        {
            // Arrange
            var contactToUpdate = new Contact()
            {
                ContactId = 3,
                Name = "Tim Cook",
                JobTitle = "CEO",
                EmailAddress = "jcook@domain.com",
                PhoneNumber = "0123789456",
                Address = "1 Apple Drive, LA",
                LastDateContacted = DateTime.Now,
                Comments = "Likes apples.",
                Company = new Company()
                {
                    CompanyId = 3,
                    CompanyName = "Apple"
                }
            };

            var updatedContact = new Contact()
            {
                ContactId = 3,
                Name = "Tim Cook",
                JobTitle = "CEO",
                EmailAddress = "jcook@domain.com",
                PhoneNumber = "0123789456",
                Address = "1 Apple Drive, LA",
                LastDateContacted = DateTime.Now,
                Comments = "Cannot stand a house with windows.",
                Company = new Company()
                {
                    CompanyId = 3,
                    CompanyName = "Apple"
                }
            };

            var mockRepository = new Mock<IContactRepository>();
            mockRepository.Setup(service => service.Update(It.IsAny<Contact>())).Returns(updatedContact);

            var contactService = new ContactService(mockRepository.Object);

            // Act
            var contact = contactService.Update(contactToUpdate);

            // Assert
            Assert.Equal(updatedContact, contact);

        }
    }
}
