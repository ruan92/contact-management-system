﻿using ContactManagementSystem.Domain.Entities;
using ContactManagementSystem.Infrastructure.Data;
using ContactManagementSystem.Infrastructure.Data.Repositories;
using ContactManagementSystem.MockEntities;
using Microsoft.EntityFrameworkCore;
using Moq;
using Moq.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UseExceptionHandlerAlternative.Middleware.GlobalErrorHandling.Exceptions;
using Xunit;

namespace ContactManagementSystem.Infrastructure.UnitTests
{
    public class ContactRepositoryTests
    {
        [Fact]
        public void GetContacts_Returns_The_Correct_Number_Of_Contacts()
        {

            // Arrange
            var dbContext = new Data.DbContext();
            var contactsToWrite = ContactsMock.GetContactsMock();

            string json = JsonConvert.SerializeObject(contactsToWrite.ToArray());
            File.WriteAllText(dbContext.ContactsDocument(), json);

            var contactRepository = new ContactRepository(dbContext);

            // Act
            var result = contactRepository.Get();

            // Assert
            var contacts = Assert.IsAssignableFrom<IList<Contact>>(result);
            Assert.Equal(3, contacts.Count);
        }

        [Fact]
        public void GetContactById_Returns_A_Contact_With_The_Given_Id()
        {
            // Arrange
            var expectedContact = new Contact()
            {
                ContactId = 2,
                Name = "Walt Disney",
                JobTitle = "Founder",
                EmailAddress = "wdisney@domain.com",
                PhoneNumber = "9876543210",
                Address = "1 Animation Road, LA",
                LastDateContacted = DateTime.Parse("2022-04-01 8:09:28 AM"),
                Comments = "Likes old Disney Movies.",
                Company = new Company()
                {
                    CompanyId = 2,
                    CompanyName = "Disney"
                }
            };

            var dbContext = new Data.DbContext();
            var contactsToWrite = ContactsMock.GetContactsMock();

            string json = JsonConvert.SerializeObject(contactsToWrite.ToArray());
            File.WriteAllText(dbContext.ContactsDocument(), json);

            var contactRepository = new ContactRepository(dbContext);

            // Act
            var result = contactRepository.Get(2);

            // Assert
            Assert.Equal(expectedContact, result);
        }

        [Fact]
        public void GetContactById_Returns_Null()
        {
            // Arrange
            var dbContext = new Data.DbContext();
            var contactsToWrite = ContactsMock.GetContactsMock();

            string json = JsonConvert.SerializeObject(contactsToWrite.ToArray());
            File.WriteAllText(dbContext.ContactsDocument(), json);

            var contactRepository = new ContactRepository(dbContext);

            // Act
            var result = contactRepository.Get(50);

            // Assert
            Assert.Null(result);

        }

        [Fact]
        public void DeleteContactById_When_Id_Exists_Produces_No_Exceptions()
        {
            // Arrange
            var dbContext = new Data.DbContext();
            var contactsToWrite = ContactsMock.GetContactsMock();

            string json = JsonConvert.SerializeObject(contactsToWrite.ToArray());
            File.WriteAllText(dbContext.ContactsDocument(), json);

            var contactRepository = new ContactRepository(dbContext);

            // Act
            contactRepository.Remove(2);

        }

        [Fact]
        public void DeleteContactById_When_Id_Not_Exists_Produces_Exception()
        {
            // Arrange
            var dbContext = new Data.DbContext();
            var contactsToWrite = ContactsMock.GetContactsMock();

            string json = JsonConvert.SerializeObject(contactsToWrite.ToArray());
            File.WriteAllText(dbContext.ContactsDocument(), json);

            var contactRepository = new ContactRepository(dbContext);

            // Assert
            Assert.Throws<CustomException>(() => contactRepository.Remove(50));
        }
    }
}
