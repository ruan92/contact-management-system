﻿using ContactManagementSystem.Domain.Entities;
using ContactManagementSystem.Infrastructure.Data;
using ContactManagementSystem.Infrastructure.Data.Repositories;
using ContactManagementSystem.MockEntities;
using Moq;
using Moq.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UseExceptionHandlerAlternative.Middleware.GlobalErrorHandling.Exceptions;
using Xunit;

namespace ContactManagementSystem.Infrastructure.UnitTests
{
    public class CompanyRepositoryTests
    {
        [Fact]
        public void GetCompanies_Returns_The_Correct_Number_Of_Companies()
        {

            // Arrange
            var dbContext = new Data.DbContext();
            var companiesToWrite = CompaniesMock.GetCompaniesMock();

            string json = JsonConvert.SerializeObject(companiesToWrite.ToArray());
            File.WriteAllText(dbContext.CompaniesDocument(), json);

            var companyRepository = new CompanyRepository(dbContext);

            // Act
            var result = companyRepository.Get();

            // Assert
            var companies = Assert.IsAssignableFrom<IList<Company>>(result);
            Assert.Equal(3, companies.Count);
        }

        [Fact]
        public void GetCompanyById_Returns_A_Company_With_The_Given_Id()
        {
            var expectedCompany = new Company()
            {
                CompanyId = 2,
                CompanyName = "Disney"
            };

            var dbContext = new Data.DbContext();
            var companiesToWrite = CompaniesMock.GetCompaniesMock();

            string json = JsonConvert.SerializeObject(companiesToWrite.ToArray());
            File.WriteAllText(dbContext.CompaniesDocument(), json);

            var companyRepository = new CompanyRepository(dbContext);

            // Act
            var result = companyRepository.Get(2);

            // Assert
            Assert.Equal(expectedCompany, result);
        }

        [Fact]
        public void GetCompanyById_Returns_Null()
        {
            var dbContext = new Data.DbContext();
            var companiesToWrite = CompaniesMock.GetCompaniesMock();

            string json = JsonConvert.SerializeObject(companiesToWrite.ToArray());
            File.WriteAllText(dbContext.CompaniesDocument(), json);

            var companyRepository = new CompanyRepository(dbContext);

            // Act
            var result = companyRepository.Get(50);

            // Assert
            Assert.Null(result);

        }

        [Fact]
        public void DeleteCompanyById_When_Id_Exists_Produces_No_Exceptions()
        {
            var dbContext = new Data.DbContext();
            var companiesToWrite = CompaniesMock.GetCompaniesMock();

            string json = JsonConvert.SerializeObject(companiesToWrite.ToArray());
            File.WriteAllText(dbContext.CompaniesDocument(), json);

            var companyRepository = new CompanyRepository(dbContext);

            // Act
            companyRepository.Remove(2);

        }

        [Fact]
        public void DeleteCompanyById_When_Id_Not_Exists_Produces_Exception()
        {
            var dbContext = new Data.DbContext();
            var companiesToWrite = CompaniesMock.GetCompaniesMock();

            string json = JsonConvert.SerializeObject(companiesToWrite.ToArray());
            File.WriteAllText(dbContext.CompaniesDocument(), json);

            var companyRepository = new CompanyRepository(dbContext);

            // Assert
            Assert.Throws<CustomException>(() => companyRepository.Remove(50));
        }

    }
}
