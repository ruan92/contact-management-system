﻿using ContactManagementSystem.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagementSystem.MockEntities
{
    public class CompaniesMock
    {
        /// <summary>
        /// Create and return mock Company objects.
        /// </summary>
        /// <returns>A list of 3 companies.</returns>
        public static IList<Company> GetCompaniesMock()
        {
            return new List<Company>
            {
                new Company()
                {
                    CompanyId = 1,
                    CompanyName = "HP"
                },
                new Company()
                {
                    CompanyId = 2,
                    CompanyName = "Disney"
                },
                new Company()
                {
                    CompanyId = 3,
                    CompanyName = "Apple"
                }
            };
        }
    }
}
