﻿using ContactManagementSystem.Domain.Entities;

namespace ContactManagementSystem.MockEntities
{
    public class ContactsMock
    {
        /// <summary>
        /// Create and return mock Contact objects.
        /// </summary>
        /// <returns>A list of 3 contacts.</returns>
        public static IList<Contact> GetContactsMock()
        {
            return new List<Contact>
            {
                new Contact()
                {
                    ContactId = 1,
                    Name = "Bill Gates",
                    JobTitle = "Founder",
                    EmailAddress = "bgates@domain.com",
                    PhoneNumber = "0123456789",
                    Address = "1 Windows Boulevard, Seattle",
                    LastDateContacted = DateTime.Parse("2022-03-01 8:09:28 AM"),
                    Comments = "Likes staring through windows.",
                    Company = new Company()
                    {
                        CompanyId = 1,
                        CompanyName = "Microsoft"
                    }
                },
                new Contact()
                {
                    ContactId = 2,
                    Name = "Walt Disney",
                    JobTitle = "Founder",
                    EmailAddress = "wdisney@domain.com",
                    PhoneNumber = "9876543210",
                    Address = "1 Animation Road, LA",
                    LastDateContacted = DateTime.Parse("2022-04-01 8:09:28 AM"),
                    Comments = "Likes old Disney Movies.",
                    Company = new Company()
                    {
                        CompanyId = 2,
                        CompanyName = "Disney"
                    }
                },
                new Contact()
                {
                    ContactId = 3,
                    Name = "Tim Cook",
                    JobTitle = "CEO",
                    EmailAddress = "jcook@domain.com",
                    PhoneNumber = "0123789456",
                    Address = "1 Apple Drive, LA",
                    LastDateContacted = DateTime.Parse("2021-04-01 8:09:28 AM"),
                    Comments = "Likes apples.",
                    Company = new Company()
                    {
                        CompanyId = 3,
                        CompanyName = "Apple"
                    }
                }
            };
        }
    }
}