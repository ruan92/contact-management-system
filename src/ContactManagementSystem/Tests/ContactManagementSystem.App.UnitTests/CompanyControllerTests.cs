﻿using ContactManagementSystem.App.Controllers;
using ContactManagementSystem.Domain.Entities;
using ContactManagementSystem.Infrastructure.Services;
using ContactManagementSystem.MockEntities;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ContactManagementSystem.App.UnitTests
{
    public class CompanyControllerTests
    {
        [Fact]
        public void GetCompanies_Returns_The_Correct_Number_Of_Companies()
        {
            // Arrange
            var mockService = new Mock<ICompanyService>();
            mockService.Setup(service => service.Get()).Returns(CompaniesMock.GetCompaniesMock());

            var controller = new CompanyController(mockService.Object);

            // Act
            var actionResult = controller.Get();

            // Assert
            var result = Assert.IsType<OkObjectResult>(actionResult.Result);
            var companies = Assert.IsAssignableFrom<IList<Company>>(result.Value);
            Assert.Equal(3, companies.Count);
        }

        [Fact]
        public void GetCompanyById_Returns_A_Company_With_The_Given_Id()
        {
            // Arrange
            var company = new Company()
            {
                CompanyId = 3,
                CompanyName = "Apple"
            };

            var mockService = new Mock<ICompanyService>();
            mockService.Setup(service => service.Get(It.IsAny<int>())).Returns(company);

            var controller = new CompanyController(mockService.Object);

            // Act
            var actionResult = controller.Get(3);

            // Assert
            var viewResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var model = Assert.IsAssignableFrom<Company>(viewResult.Value);
            Assert.Equal(model, company);

        }

        [Fact]
        public void GetCompanyById_Returns_A_Not_Found_With_The_Given_Id()
        {
            var mockService = new Mock<ICompanyService>();
            mockService.Setup(service => service.Get(It.IsAny<int>())).Returns((Company)null);

            var controller = new CompanyController(mockService.Object);

            // Act
            var actionResult = controller.Get(1);

            // Assert
            var viewResult = Assert.IsType<NotFoundResult>(actionResult.Result);

        }

        [Fact]
        public void CreateCompany_ReturnsACreatedAtRouteResultAndAddsCompany_WhenModelStateIsValid()
        {
            // Arrange
            var company = new Company()
            {
                CompanyId = 3,
                CompanyName = "Apple"
            };

            var mockService = new Mock<ICompanyService>();
            mockService.Setup(service => service.Create(It.IsAny<Company>()))
                .Returns(company);

            var controller = new CompanyController(mockService.Object);

            // Act
            var actionResult = controller.Post(company);

            // Assert
            var viewResult = Assert.IsType<CreatedAtRouteResult>(actionResult.Result);
            var model = Assert.IsAssignableFrom<Company>(viewResult.Value);
            Assert.Equal(model, company);
        }

        [Fact]
        public void UpdateCompany_Returns_The_Updated_Company()
        {
            // Arrange
            var companyToUpdate = new Company()
            {
                CompanyId = 3,
                CompanyName = "Apple"
            };

            var updatedCompany = new Company()
            {
                CompanyId = 3,
                CompanyName = "Apple"
            };

            var mockService = new Mock<ICompanyService>();
            mockService.Setup(service => service.Update(It.IsAny<Company>())).Returns(updatedCompany);

            var controller = new CompanyController(mockService.Object);

            // Act
            var actionResult = controller.Put(companyToUpdate);

            // Assert
            var viewResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var model = Assert.IsAssignableFrom<Company>(viewResult.Value);
            Assert.Equal(model, updatedCompany);

        }
    }
}
