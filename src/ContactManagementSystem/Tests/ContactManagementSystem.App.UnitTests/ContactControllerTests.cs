﻿using ContactManagementSystem.App.Controllers;
using ContactManagementSystem.Domain.Entities;
using ContactManagementSystem.Infrastructure.Services;
using ContactManagementSystem.MockEntities;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ContactManagementSystem.App.UnitTests
{
    public class ContactControllerTests
    {
        [Fact]
        public void GetContacts_Returns_The_Correct_Number_Of_Contacts()
        {
            // Arrange
            var mockService = new Mock<IContactService>();
            mockService.Setup(service => service.Get()).Returns(ContactsMock.GetContactsMock());

            var controller = new ContactController(mockService.Object);

            // Act
            var actionResult = controller.Get();

            // Assert
            var result = Assert.IsType<OkObjectResult>(actionResult.Result);
            var contacts = Assert.IsAssignableFrom<IList<Contact>>(result.Value);
            Assert.Equal(3, contacts.Count);
        }


        [Fact]
        public void GetContactById_Returns_A_Contact_With_The_Given_Id()
        {
            // Arrange
            var contact = new Contact()
            {
                ContactId = 3,
                Name = "Tim Cook",
                JobTitle = "CEO",
                EmailAddress = "jcook@domain.com",
                PhoneNumber = "0123789456",
                Address = "1 Apple Drive, LA",
                LastDateContacted = DateTime.Now,
                Comments = "Likes apples.",
                Company = new Company()
                {
                    CompanyId = 3,
                    CompanyName = "Apple"
                }
            };

            var mockService = new Mock<IContactService>();
            mockService.Setup(service => service.Get(It.IsAny<int>())).Returns(contact);

            var controller = new ContactController(mockService.Object);

            // Act
            var actionResult = controller.Get(3);

            // Assert
            var viewResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var model = Assert.IsAssignableFrom<Contact>(viewResult.Value);
            Assert.Equal(model, contact);

        }

        [Fact]
        public void GetContactById_Returns_A_Not_Found_With_The_Given_Id()
        {
            var mockService = new Mock<IContactService>();
            mockService.Setup(service => service.Get(It.IsAny<int>())).Returns((Contact)null);

            var controller = new ContactController(mockService.Object);

            // Act
            var actionResult = controller.Get(1);

            // Assert
            var viewResult = Assert.IsType<NotFoundResult>(actionResult.Result);

        }

        [Fact]
        public void CreateContact_ReturnsACreatedAtRouteResultAndAddsContact_WhenModelStateIsValid()
        {
            // Arrange
            var contact = new Contact()
            {
                ContactId = 3,
                Name = "Tim Cook",
                JobTitle = "CEO",
                EmailAddress = "jcook@domain.com",
                PhoneNumber = "0123789456",
                Address = "1 Apple Drive, LA",
                LastDateContacted = DateTime.Now,
                Comments = "Likes apples.",
                Company = new Company()
                {
                    CompanyId = 3,
                    CompanyName = "Apple"
                }
            };

            var mockService = new Mock<IContactService>();
            mockService.Setup(service => service.Create(It.IsAny<Contact>()))
                .Returns(contact);

            var controller = new ContactController(mockService.Object);

            // Act
            var actionResult = controller.Post(contact);

            // Assert
            var viewResult = Assert.IsType<CreatedAtRouteResult>(actionResult.Result);
            var model = Assert.IsAssignableFrom<Contact>(viewResult.Value);
            Assert.Equal(model, contact);
        }

        [Fact]
        public void UpdateContact_Returns_The_Updated_Contact()
        {
            // Arrange
            var contactToUpdate = new Contact()
            {
                ContactId = 3,
                Name = "Tim Cook",
                JobTitle = "CEO",
                EmailAddress = "jcook@domain.com",
                PhoneNumber = "0123789456",
                Address = "1 Apple Drive, LA",
                LastDateContacted = DateTime.Now,
                Comments = "Likes apples.",
                Company = new Company()
                {
                    CompanyId = 3,
                    CompanyName = "Apple"
                }
            };

            var updatedContact = new Contact()
            {
                ContactId = 3,
                Name = "Tim Cook",
                JobTitle = "CEO",
                EmailAddress = "jcook@domain.com",
                PhoneNumber = "0123789456",
                Address = "1 Apple Drive, LA",
                LastDateContacted = DateTime.Now,
                Comments = "Cannot stand a house with windows.",
                Company = new Company()
                {
                    CompanyId = 3,
                    CompanyName = "Apple"
                }
            };

            var mockService = new Mock<IContactService>();
            mockService.Setup(service => service.Update(It.IsAny<Contact>())).Returns(updatedContact);

            var controller = new ContactController(mockService.Object);

            // Act
            var actionResult = controller.Put(contactToUpdate);

            // Assert
            var viewResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var model = Assert.IsAssignableFrom<Contact>(viewResult.Value);
            Assert.Equal(model, updatedContact);

        }
    }
}
